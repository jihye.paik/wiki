---
title: "blue cats2"
content_template: templates/concept
weight: 10
---
{{% capture overview %}}

README

{{% /capture %}}

{{% capture body %}}
# BLUE CATS 프로젝트 헌장
{{% /capture %}}

## **목차**
1. hello
2. **[개요](#1-)**
3. **[개발 전략](#2--)**
4. **[로드맵](#3-)**
5. **[요구사항](voc.html)**
6. **[아키텍처](ARCHITECTURE.md)**
7. **[스프린트](SPRINT.md)** 

<br/>

## **1. 개요**
- 미션 test
  - **BLUE CATS는 Cloud-Native Analytics Platform이다.**  
    - Private Cloud 환경을 제공한다.
    - Distributed System Architecture과 Event-Driven Microservice Architecture을 정의한다.
    - 통합 분석 환경이다.
- 프로젝트 구성원
  - PM: 고형호
  - PL: 심승용, 이종표
  - 팀원: 최영헌, 문성은, 박영석, 최지영, 백지혜
- 고객
  - 미래로시스템 임직원
  - 고객사

<br/>

## **2. 개발 전략**
- **오픈 소스 이해와 조합으로 개발한다.**  
  이해와 조합 : [-구현(코딩)-] = 80% : [-20%-]  
  **파레토 법칙**(Pareto principle, 80 대 20 법칙)은 '**전체 결과의 80%가 전체 원인의 20%에서 일어나는 현상**'을 가리킨다.  
  ![Summary](../images/1.JPG)

<br/>

## **3. 로드맵**
- **[+시스템 모니터링 v1.x.x+]** > 시스템 관리 v2.x.x > 시스템 분석
- 비즈니스 모니터링 > 비즈니스 관리 > 비즈니스 분석

### 3.1 공청회
- [x] 1차 공청회: 2019-09-24(화) 10:00 ~ 11:40 #429
- [x] 2차 공청회: 2019-11-20(수) 10:00 ~ 11:30 #446

### 3.2 적용
- [x] 2019년 07월 - DMS Lot Tracing
- [x] [2019년 11월 - 리소스](./Docs/%EC%A0%81%EC%9A%A9/%EB%A6%AC%EC%86%8C%EC%8A%A3.md)
- [ ] [2019년 12월 - Foundry S1, Memory 16라인](./Docs/%EC%A0%81%EC%9A%A9/FoundryS1_and_Memory16Line.md)
- [ ] 2019년 11월 - HOYA 전자, PCC
- [ ] MSS
- [ ] MiDEWS
- [ ] 사내 로그 통합 
- [ ] 스프린트(프로젝트 관리)

### 3.3 시스템 모니터링 v1.x.x
- **라이선스**: [Apache License 2.0](https://github.com/opendistro-for-elasticsearch/opendistro-build/blob/master/LICENSE), [MIT](https://github.com/cdr/code-server/blob/master/LICENSE), [GPL 2.0](https://github.com/acassen/keepalived/blob/master/COPYING)  
  ![Summary](../images/2.JPG)
- **로드맵**
  - **v1.0.0 - Upcomming** **(2020년 4분기)**
     - 목표: 추적 데이터(액터 메시지) 수집
     - 가치: Observability(관측 가능성) 데이터 통합
   - **v0.7.0 - Upcomming** **(2020년 3분기)**
     - 목표: 흐름 기반 프로그래밍
     - 가치: 시각화 로직 구현
   - **v0.6.0 - Upcomming** **(2020년 2분기)**
     - 목표: 클러스터 모니터링 및 관리 고도화
     - 가치: End-To-End Cluster 운영 환경 제공
   - **[v0.3.0 - 2019년 12월](./BLUE%20CATS%20v0.3.0/README.md)**
     - 목표: Distributed System Architecture 고도화
     - 가치: 시스템 안정화(99.999% 가동률)
   - **[v0.2.0 - 2019년 10월](./BLUE%20CATS%20v0.2.0/README.md)**  
     - 목표: Apache License 2.0
     - 가치: "오픈 소스 이해와 조합"으로 첫 공식 배포
   - **[v0.1.0 - 2019년 05월](./BLUE%20CATS%20v0.1.0/README.md)**
     - 목표: 분석 플랫폼 구축
     - 가치: MLS 시범 적용
- **주요 요구사항**
   - 장애 알림
   - Observability(관측 가능성) 데이터 수집
     - 지표 데이터(CPU, 메모리, 디스크, 네트워크, 파일 시스템, 컨테이너, 액터)
     - 추적 데이터(메시지)
     - 로그 데이터  
  ![Summary](../images/3.JPG)
   - 시스템 모니터링 업무 프로세스 개선  
  ![Summary](../images/4.JPG)
