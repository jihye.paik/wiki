---
title: "VOC"
content_template: templates/concept
weight: 10
---
{{% capture overview %}}

Business Leading Universal Environment Collecting Analysis Tracing System , 운영 데이터 분석 플랫폼 기술연구

{{% /capture %}}

{{% capture body %}}
# BLUE CATS 요구사항
{{% /capture %}}

# **BLUE CATS 요구사항**

## **목차**
1. **[알람 개요](#1-알람-개요)**
1. **[알람 조건](#2-알람-조건)**
1. **[모니터링 정보](#3-모니터링-정보)**
1. **[용어](#4-용어)**

<br/>

## 1 알람 개요
1. 알람 통지
   - **[5] : CS 통지, 고객 통지 - 공정을 중단 시킨다.**
   - **[4] : CS 통지, 고객 통지 - 공정을 지연 시킨다.**
   - [3] : CS 통지
   - [2] : CS 통지
   - [1] : CS 통지
1. 알람 형식
   > **[수준][사이트][라인][제품][서버][범주] 제목**
   - 수준: Fatal > Error > Warnning > Info > Debug
   - 사이트: SEC(Samsung Electronics), SDC(Samsung Display), ...
   - 라인: SAS, S1, ...
   - 제품: DM61, ...
   - 서버: DBA, DBS, Loader1,2, INSP01, ...
   - 범주: CPU, Database, Disk, File, Folder, GPU, Hardware, Log, Memory, Network, Spike, Uptime
1. 알람 절차  
   <img src="/Docs/Images/Alert_Process.png" width="80%" height="80%">
1. [알람 대시보드](http://192.168.70.23/app/kibana#/dashboard/9c3a5730-0a97-11ea-94e7-4bbe4e8025f4)

## 2 알람 조건
1. Container
1. CPU [대시보드1](http://192.168.70.23/app/kibana#/dashboard/Metricbeat-system-overview-ecs) [대시보드2](http://192.168.70.23/app/kibana#/dashboard/79ffd6e0-faa0-11e6-947f-177f697178b8-ecs)
   1. CPU 사용률
      - [x] [+1차+] - **CPU 사용률**이 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - #433, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/LqADfG4BI7GL_Q85SPhN)
        - [4] 99%를 10분이상 유지하면 알람한다. 
1. Database
   1. Connection
      - [ ] 3차 - **DB와 접속되지 않으면** 알람한다.
        - [5] DB서버에서 DB를 접속해 본다(네트워크 문제를 배제할 수 있다).
   1. MS-SQL
1. Disk [대시보드](http://192.168.70.23/app/kibana#/dashboard/79ffd6e0-faa0-11e6-947f-177f697178b8-ecs)
   1. Disk 사용률
      - [x] [+1차+] - 특정 드라이브의 **Disk 사용률**이 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - #433, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/bKE0fG4BI7GL_Q85CQP8)
        - [5] 90% 이상 사용 (D, E, F, G 드라이브)
   1. Disk 사용 가능 용량
      - [x] [+1차+] - **특정 드라이브의 Disk 사용 가능 용량**(**MB**) 이 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - #433, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/h6AhfG4BI7GL_Q85Vv49)
        - [5] Rose H/A에서 디스크 용량이 1GB 이하인 경우 미러링이 중지된다(D, E, F, G 드라이브).
1. File [대시보드](http://192.168.70.23/app/kibana#/dashboard/249feb50-0780-11ea-a4db-bf372325e48a)
   1. File 개수
      - [x] [+1차+] - 감시 폴더의 **파일 개수**가 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - #438, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/pqEkfW4BI7GL_Q85oLcw)
        - EventManager 멈출 시 파일 적재를 감지하기 위해 사용(확장자로 감지)한다.
        - [4] SMF 파일이  25개 이상일 경우 알람한다.
        - [4] 1~4시간 동안 폴더에 파일의 변동이 없는 경우 알람한다.
   1. File 크기
      - [x] [+1차+] - 감시 **파일 크기**(**KB**)가 **설정 크기**(**KB**) **보다 크면** 알람한다. 
        - #438, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/JbuFfm4BI7GL_Q85groT)
        - [3] MiDEWS 3.0의 경우 500MB 이상 파일을 입력 받으면 다운된다.
   1. File 편집 시간 
      - [x] 감시 폴더의 **파일 편집 시간**이 **설정 시간을 초과하면** 알람한다. 
        - #438, [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/3NLMgm4BI7GL_Q85brrS)
        - *Ex. Log 파일의 마지막 수정 시간이 5분 이상 지났을 때 장애로 판단하여 알람한다.*
        - [3] lot.end 감지 (1분 이상 유지되는 경우 장애)
        - MiDEWS의 서비스(SRA, SCB, ...)가 로그를 갱신하지 않는 경우 알람한다.   
          MiDEWS SPR(Serivce Provider Restart)이 이미 하고 있다. SPR이 현재 문제가 있다(2012이상에서 오동작).  
1. Folder [대시보드](http://192.168.70.23/app/kibana#/dashboard/249feb50-0780-11ea-a4db-bf372325e48a)
   1. Folder 개수
      - [x] [+1차+] - 감시 폴더 내에 있는 **서브 폴더의 개수**가 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - #438 [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/oaEqfW4BI7GL_Q85O89a)
        - [3] 에러폴더, 타임아웃 폴더 : 에러 발생 시 적재한다.
        - [4] LateFTP 폴더: 날짜 별로 적재한다.
        - [5] Wait 폴더, ReWork 폴더: 특정 개수 이상 적재 시 데이터 지연이 발생한다.
1. GPU
   1. GPU 사용률
      - [x] 3차 - **GPU 사용률**이 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
1. Hardware [대시보드](http://192.168.70.23/goto/19d6645b27e76dadc5f4ab881291d441?security_tenant=private)
   1. HP 서버
      - [ ] [+1차+] - **HP 서버**에서 **H/W 장애**(**SNMP Trap, PredictFailure**)**를 발생하면** 알람한다. 
        - *Ex. SNMP Trap란? 제조사 Agent에 의해 자의적으로 생성/송신하는 장애 SNMP 메세지 중 하나다.* 
        - HP 서버, Array Controller 정보를 얻어온다. Smart Storage 
   1. Pure Storage 
      - [x] [+1차+] - **Pure Storage**에서 **H/W 장애**(**SNMP Trap**)**를 발생하면** 알람한다. 
        - *Ex. Pure Storage Log 파일이 있고, Critical 심각도를 가진 Alert Message Check라는 키워드를 갖는 Alert Message가 존재하면 알람한다.*
   1. Rose H/A(Failover)
      - [ ] [+1차+] - **설정한 IP에서 Failover가 발생하면** 알람한다.
        - [5] Rose H/A의 로그에 특정 문구로 확인 가능하다. 
1. Log [대시보드](http://192.168.70.23/app/kibana#/dashboard/9a99c0f0-0b23-11ea-94e7-4bbe4e8025f4)
   1. 연산 오류
      - [x] [+1차+] - **Log 파일**에서 **특정 텍스트 구문을 발견하면** 알람한다.
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/LeYihm4BI7GL_Q85O6Da)
   1. 연산 시간
      - [ ] [+1차+] - **MiDEWS 연산 구간별 처리 시간**이 **설정 시간을 초과하면** 알람한다. 
   1. Windows Log 개수
      - [x] 2차 - **Windows Log의 Error/Warning 개수**가 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상 충족**하면 알람한다. 
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/M9png24BI7GL_Q85KF3U)
        - *Ex. 24시간 이내의 Windows Log에서 Error가 5개 이상 발생하면 알람한다.*
        - 특정 에러 로그는 무시할 수 있어야 한다.
1. Memory [대시보드](http://192.168.70.23/app/kibana#/dashboard/79ffd6e0-faa0-11e6-947f-177f697178b8-ecs)
   1. Memory 사용률
      - [x] 2차 - **Memory 사용률**이 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다. 
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/xqN9fW4BI7GL_Q85ZClW)
        - Physical Memory와 Virtual Memory를 별도 표시해야 한다.
1. Network [대시보드1](http://192.168.70.23/app/kibana#/dashboard/b9a0bcf0-0b5b-11ea-94e7-4bbe4e8025f4), [대시보드2](http://192.168.70.23/goto/6c679d9dd4604d35a7d6b82636b3df41?security_tenant=private)
   1. Remote 드라이브
      - [x] [+1차+] - 감시 **공유 폴더**에 **접근할 수 없다면** 알람한다.
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/-NyYg24BI7GL_Q85en2I)
        - [5] DM에서 설비를 원격 드라이브(Z, Y 드라이브)로 연결하여 데이터를 가져온다.
   1. Port 상태
      - [x] [+1차+] - 감시 **Port 연결이 안되면(닫힌 상태면)** 알람한다.
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/Xdlbg24BI7GL_Q85Xt7Z)
   1. Port 개수 
      - [x] **열린 Port 개수**가 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다(Port Full). 
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/odPWgm4BI7GL_Q85AE2E)
        - [4] IIS에서 지정한 최대 사용 포트수를 넘어가면 포트 풀(Port Full) 상태가 된다.
      - [x] **대기(TIME_WAIT) Port 개수**가 설정 조건과 비교 했을 때 **최소 유지 시간(분) 이상이면** 알람한다(Port Full). 
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/UtX_gm4BI7GL_Q85q63O)
        - *Ex. TIME_WAIT Port가 증가되면 서버의 소켓이 고갈되어 커넥션 타임아웃이 발생한다.*
   1. FTP
   1. SMB
   1. NFS
   1. RSHD
1. Spike [대시보드](http://192.168.70.23/goto/d9bddf4816ec45badf901ae10c658a1a?security_tenant=private)
   1. 이동 평균
      - [ ] **이동 평균**을 벗어나는 **최소 유지 시간(분) 이상이면** 알람한다.
        - #431
1. Uptime
   1. OS
      - [ ] 2차 - **Windows 서버의 마지막 부팅 시간**이 **최소 가동 시간(일) 이상이면** 알람한다. 
        - [데모](http://192.168.70.23/app/opendistro-alerting#/monitors/GN_Qg24BI7GL_Q85ESM3)
        - 가동 시간(일) 기준 값 : 360일
        - [3] Windows의 가동 시간(일)이 360일 이상 이면 알람한다.
1. Virtual Machine(VMware)

<br />

## **3. 모니터링 정보**
1. Container
1. CPU
   1. CPU 사용률
      - TopN 프로세스 수집에 Check 가 된 경우 레시피 상세내역의 부가 정보에 CPU 사용률(%)이 출력된다.
1. Database
1. Disk
1. File
   1. File 개수
      - TopN 프로세스 수집에 Check 가 된 경우 레시피 상세내역의 부가 정보에 파일 속성이 수집되어 부가 정보에 출력된다.
   1. File 편집 시간
   1. File 크기
1. Folder
   1. Folder 개수
      - 입력한 감시 폴더가 존재하지 않을 경우 정상으로 판단한다.
      - TopN 프로세스 수집에 Check 가 된 경우 레시피 상세내역의 부가 정보에 폴더 속성이 수집되어 부가 정보에 출력된다.
1. GPU
1. Hardware
1. Log
1. Memory
   1. Memory 사용률
      - TopN 프로세스 수집에 Check 가 된 경우 레시피 상세내역의 부가 정보에 Memory 사용률(%)이 출력된다.
1. Network
1. Spike
1. Uptime

<br/>

## **4. 용어**
- 사용률 : 비교할 사용률(%)
- 유지 시간 : 장애로 판단되는 최소 유지 시간(분)

<br />

